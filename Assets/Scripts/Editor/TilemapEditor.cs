using UnityEditor;
using UnityEngine;
using UnityEditor.UIElements;
using UnityEngine.UIElements;
using System;
using System.Collections.Generic;
using System.Collections;
using System.Linq;
using Unity.EditorCoroutines.Editor;

public class TilemapEditor : EditorWindow
{
    private TileData currentTile = null;
    private TileMapData currentMap = null;
    private TileData[] tilesData = null;
    private TileMapData[] mapsData = null;
    private TileMapData.MapVector[] mapData = null;
    private Image selectedTilePreview = null;
    private EnumField editor_mode_enumField;
    private ListView tilesListView = null;
    private ListView tileMapsListView = null;
    private VisualElement left_panel = null;
    private Label tilesLabel = null;
    private Label mapsLabel = null;
    private Button generateMapBtn = null;
    private Button overwriteMap = null;
    private TextField generatedMapTextField = null;
    private string generatedMapName = "";
    private List<Box> gridBoxes;
    private TilemapEditorMode currentEditingMode = TilemapEditorMode.Paint;
    private int largestUniqueId = 0;
    private delegate void TileEditorCopyDelegate(ref TilesEditorData[] newData, TilesEditorData[] originalData);

    private const int gridTileWidth = 64;
    private const int mapSize = 10;
    private const int mapEmptyTileId = 999;

    [MenuItem("Tools/Tilemap Editor")]
    public static void ShowExample()
    {
        TilemapEditor wnd = GetWindow<TilemapEditor>();
        wnd.titleContent = new GUIContent("Tilemap Editor");
    }

    private enum TilemapEditorMode
    {
        Paint = 0,
        Erase = 1
    }

    public void OnEnable()
    {
        // Reset variables
        currentEditingMode = TilemapEditorMode.Paint;
        selectedTilePreview = null;
        tilesListView = null;
        tileMapsListView = null;
        currentTile = null;
        currentMap = null;

        #region Main Panel
        VisualElement root = rootVisualElement;

        VisualElement main_panel = new VisualElement();
        main_panel.style.flexGrow = 1.0f;
        main_panel.style.flexDirection = FlexDirection.Row;
        main_panel.style.borderTopWidth = main_panel.style.borderRightWidth = main_panel.style.borderBottomWidth = main_panel.style.borderLeftWidth = 5.0f;
        main_panel.style.borderTopColor = main_panel.style.borderRightColor = main_panel.style.borderBottomColor = main_panel.style.borderLeftColor = Color.black;
        root.Add(main_panel);
        #endregion

        #region Left Panel
        // Setup left panel
        left_panel = new VisualElement();
        left_panel.style.flexDirection = FlexDirection.Column;
        left_panel.style.flexGrow = 0.25f;
        left_panel.style.borderTopWidth = left_panel.style.borderRightWidth = left_panel.style.borderBottomWidth = left_panel.style.borderLeftWidth = 5.0f;
        left_panel.style.borderTopColor = left_panel.style.borderRightColor = left_panel.style.borderBottomColor = left_panel.style.borderLeftColor = Color.blue;
        main_panel.Add(left_panel);

        // Setup editor mode
        editor_mode_enumField = new EnumField("Editing Mode: ", TilemapEditorMode.Paint);
        editor_mode_enumField.RegisterValueChangedCallback<System.Enum>(
            (_evt) =>
            {
                currentEditingMode = (TilemapEditorMode)_evt.newValue;
            }
        );

        left_panel.Add(editor_mode_enumField);

        // Setup selected tile preview
        left_panel.Add(new Label("Selected tile preview:"));
        Box previewBox = new Box();
        int size = 128;
        previewBox.style.minWidth = size;
        previewBox.style.minHeight = size;
        previewBox.style.maxWidth = size;
        previewBox.style.maxHeight = size;
        previewBox.style.borderTopColor = previewBox.style.borderRightColor = previewBox.style.borderBottomColor = previewBox.style.borderLeftColor = Color.blue;
        left_panel.Add(previewBox);

        selectedTilePreview = new Image();
        selectedTilePreview.style.flexShrink = 1.0f;
        previewBox.Add(selectedTilePreview);

        GenerateTilesListView(left_panel);
        GenerateMapsListView(left_panel);

        generatedMapTextField = new TextField("Generated Map name: ")
        {
            viewDataKey = "input"
        };

        generatedMapTextField.RegisterCallback<ChangeEvent<string>>(
            (_evt) =>
            {
                generatedMapName = _evt.newValue;
            }
        );

        left_panel.Add(generatedMapTextField);

        generateMapBtn = new Button()
        {
            text = "Generate Map",
            style =
            {
                width = 100,
                height = 50
            }
        };

        generateMapBtn.clicked += OnMapGenerate;

        left_panel.Add(generateMapBtn);

        overwriteMap = new Button()
        {
            text = "Overwrite Map",
            style =
            {
                width = 100,
                height = 50
            }
        };

        overwriteMap.clicked += OnMapOverwrite;

        left_panel.Add(overwriteMap);

        #endregion

        #region Right Panel
        // Setup right panel
        VisualElement right_panel = new VisualElement();
        right_panel.style.flexDirection = FlexDirection.Column;
        right_panel.style.flexGrow = 1f;
        right_panel.style.borderTopWidth = right_panel.style.borderRightWidth = right_panel.style.borderBottomWidth = right_panel.style.borderLeftWidth = 5.0f;
        right_panel.style.borderTopColor = right_panel.style.borderRightColor = right_panel.style.borderBottomColor = right_panel.style.borderLeftColor = Color.red;
        main_panel.Add(right_panel);

        gridBoxes = new List<Box>();

        // Setup tilemap grid
        for (int j = 0; j < mapSize; j++)
        {
            VisualElement image_row_panel = new VisualElement();
            image_row_panel.style.flexDirection = FlexDirection.Row;
            right_panel.Add(image_row_panel);

            for (int i = 0; i < mapSize; i++)
            {
                Box box = new Box();
                box.style.minWidth = gridTileWidth;
                box.style.minHeight = gridTileWidth;
                box.style.maxWidth = gridTileWidth;
                box.style.maxHeight = gridTileWidth;
                box.style.borderTopColor = box.style.borderRightColor = box.style.borderBottomColor = box.style.borderLeftColor = Color.blue;
                box.RegisterCallback<MouseUpEvent>(OnMouseUpEvent);
                image_row_panel.Add(box);

                Image image = new Image();
                image.style.flexShrink = 1.0f;
                image.image = null;
                box.Add(image);

                // Cache the y coordinate in the tab index as a workaround for a Unity glitch always returning y as 0.
                box.tabIndex = (j * gridTileWidth);
                gridBoxes.Add(box);
            }
        }

        InitializeMapData();
        RefreshLargestUniqueId();
        #endregion
    }

    private void OnFocus()
    {
        RefreshUI();
    }

    private void RefreshUI()
    {
        if (left_panel == null) return;

        GenerateTilesListView(left_panel);
        GenerateMapsListView(left_panel);

        // TODO : Needs refactoring.
        if (generatedMapTextField != null)
        {
            left_panel.Remove(generatedMapTextField);
            left_panel.Add(generatedMapTextField);
        }

        if (generateMapBtn != null)
        {
            left_panel.Remove(generateMapBtn);
            left_panel.Add(generateMapBtn);
        }

        if (overwriteMap != null)
        {
            left_panel.Remove(overwriteMap);
            left_panel.Add(overwriteMap);
        }
    }

    private void GenerateTilesListView(VisualElement panel)
    {
        GenerateListView<TileData>(panel, ref tilesListView, ref tilesLabel, "Tiles:", OnTileSelectionChange, ref tilesData, CopyTilesData);
    }

    private void GenerateMapsListView(VisualElement panel)
    {
        GenerateListView<TileMapData>(panel, ref tileMapsListView, ref mapsLabel, "Maps:", OnMapSelectionChange, ref mapsData, CopyTileMapsData);
    }

    private void GenerateListView<T>(
        VisualElement panel,
        ref ListView listView,
        ref Label label,
        string labelText,
        Action<IEnumerable<object>> evt,
        ref T[] dataObjects,
        TileEditorCopyDelegate copyEditorData) where T : TilesEditorData
    {
        if (listView != null)
            panel.Remove(listView);

        if (label != null)
            panel.Remove(label);

        label = new Label(labelText);
        panel.Add(label);

        dataObjects = GetAllInstances<T>();

        TilesEditorData[] temp = new TilesEditorData[dataObjects.Length];
        copyEditorData(ref temp, dataObjects);

        Func<VisualElement> makeItem = () => new Label();
        Action<VisualElement, int> bindItem = (e, i) => (e as Label).text = temp[i].GetName();

        const int itemHeight = 16;
        listView = new ListView(temp, itemHeight, makeItem, bindItem);

        listView.selectionType = SelectionType.Multiple;
        listView.onSelectionChange += objects => evt(objects);

        listView.style.flexGrow = 1.0f;

        panel.Add(listView);
    }

    private void OnTileSelectionChange(IEnumerable<System.Object> obj)
    {
        List<System.Object> tiles = obj.ToList();

        if (tiles != null && tiles.Count > 0)
        {
            currentTile = tiles[0] as TileData;
        }

        if (currentTile == null) return;

        Sprite tileSprite = currentTile.GetSprite();
        selectedTilePreview.image = null;
        selectedTilePreview.image = currentTile.GetSprite().texture;

        selectedTilePreview.sourceRect = GetSourceRect(tileSprite);

        if (currentEditingMode == TilemapEditorMode.Erase)
        {
            // Switch back to pain if last mode was erase when selecting a new tile
            editor_mode_enumField.value = TilemapEditorMode.Paint;
            currentEditingMode = TilemapEditorMode.Paint;
        }
    }

    private void OnMapSelectionChange(IEnumerable<System.Object> obj)
    {
        List<System.Object> maps = obj.ToList();

        if (maps != null && maps.Count > 0)
        {
            currentMap = maps[0] as TileMapData;
        }

        if (currentMap == null) return;

        for (int i = 0; i < gridBoxes.Count; i++)
        {
            gridBoxes[i].Clear();
        }

        LoadMapTiles();
    }

    private void InitializeMapData()
    {
        mapData = new TileMapData.MapVector[mapSize];

        for (int i = 0; i < mapData.Length; i++)
        {
            mapData[i].Y = new int[mapSize];

            for (int j = 0; j < mapData[i].Y.Length; j++)
            {
                mapData[i].Y[j] = mapEmptyTileId;
            }
        }
    }

    private void LoadMapTiles()
    {
        mapData = currentMap.GetMapData();

        if (mapData == null || mapData.Length == 0)
        {
            InitializeMapData();
            return;
        }

        for (int x = 0; x < mapData.Length; x++)
        {
            for (int y = 0; y < mapData[x].Y.Length; y++)
            {
                int id = mapData[x].Y[y];
                RenderLoadedTile(x, y, id);
            }
        }
    }

    private void DebugMapData()
    {
        for (int i = 0; i < mapData.Length; i++)
        {
            for (int j = 0; j < mapData[i].Y.Length; j++)
            {
                Debug.Log("x " + i + " y " + j + " id " + mapData[i].Y[j]);
            }
        }
    }

    private void RenderLoadedTile(int x, int y, int tileId)
    {
        if (tileId == mapEmptyTileId) return;

        string indexStr = y.ToString() + x.ToString();
        int index = int.Parse(indexStr);
        gridBoxes[index].Add(GetTileImageByTileId(tileId));
    }

    private Image GetTileImageByTileId(int tileId)
    {
        Image image = new Image();

        Sprite tileSprite;

        for (int i = 0; i < tilesData.Length; i++)
        {
            if (tilesData[i].GetId() == tileId)
            {
                TileData data = tilesData[i];
                image.style.flexShrink = 1.0f;
                tileSprite = data.GetSprite();
                image.image = tileSprite.texture;
                image.sourceRect = GetSourceRect(tileSprite);
                break;
            }
        }

        return image;
    }

    private Rect GetSourceRect(Sprite sprite)
    {
        Rect rect = new Rect(sprite.rect.x, sprite.texture.height - (sprite.rect.y + sprite.rect.height), sprite.rect.width, sprite.rect.height);

        return rect;
    }

    private void OnMouseUpEvent(MouseUpEvent _event)
    {
        if (_event.target.GetType() == typeof(Image) && currentEditingMode == TilemapEditorMode.Erase)
        {
            Image image = _event.target as Image;
            if (image != null)
            {
                VisualElement parent = image.parent;
                parent.Clear();

                SetMapCoordinates((int)parent.localBound.x, parent.tabIndex, mapEmptyTileId);
            }
        }
        else if (_event.target.GetType() == typeof(Box) && currentEditingMode == TilemapEditorMode.Paint)
        {
            if (currentTile == null) return;

            Box box = _event.target as Box;

            Image image = new Image();
            image.style.flexShrink = 1.0f;
            Sprite tileSprite = currentTile.GetSprite();
            image.image = tileSprite.texture;
            image.sourceRect = GetSourceRect(tileSprite);
            box.Add(image);
            // Read cached y coordinate in the tab index as a workaround for a Unity glitch always returning y as 0.
            // Debug.Log(box.localBound.x + "  " + box.localBound.y + "  " + box.tabIndex);
            SetMapCoordinates((int)box.localBound.x, box.tabIndex, currentTile.GetId());
        }
        else if (_event.target.GetType() == typeof(Image) && currentEditingMode == TilemapEditorMode.Paint)
        {
            if (currentTile == null) return;

            Image image = _event.target as Image;
            if (image != null)
            {
                Sprite tileSprite = currentTile.GetSprite();
                image.image = tileSprite.texture;
                image.sourceRect = GetSourceRect(tileSprite);
                VisualElement parent = image.parent;
                parent.Add(image);

                SetMapCoordinates((int)parent.localBound.x, parent.tabIndex, currentTile.GetId());
            }
        }
    }

    private void OnMapGenerate()
    {
        TileMapData generatedMap = CreateInstance(typeof(TileMapData)) as TileMapData;

        generatedMap.SetName(generatedMapName);

        if (currentMap != null)
        {
            generatedMap.SetMapData(currentMap.GetMapData());
        }
        else
        {
            generatedMap.SetMapData(mapData);
        }

        largestUniqueId++;
        generatedMap.SetId(largestUniqueId);
        AssetDatabase.CreateAsset(generatedMap, "Assets/ScriptableObjects/Maps/" + generatedMapName + ".asset");
        RefreshUI();
        EditorCoroutineUtility.StartCoroutine(SelectNewMap(), this);
    }

    private IEnumerator SelectNewMap()
    {
        yield return new WaitForSeconds(0.1f);
        int selectionIndex = GetMapListViewElementIndexByMapId(largestUniqueId);
        tileMapsListView.AddToSelection(selectionIndex);
    }

    private IEnumerator SelectCurrentMap()
    {
        yield return new WaitForSeconds(0.1f);
        int selectionIndex = GetMapListViewElementIndexByMapId(currentMap.GetId());
        tileMapsListView.AddToSelection(selectionIndex);
    }

    private void OnMapOverwrite()
    {
        if (currentMap == null) return;

        TileMapData newData = CreateInstance<TileMapData>();
        newData = GetMapDataById(currentMap.GetId());
        newData.SetMapData(mapData);
        EditorUtility.SetDirty(newData);
        CopyTileMapData(ref currentMap, newData);
        RefreshUI();
        EditorCoroutineUtility.StartCoroutine(SelectCurrentMap(), this);
    }

    private void SetMapCoordinates(int xPos, int yPos, int tileId)
    {
        int x = (int)((float)xPos / gridTileWidth);
        int y = (int)((float)yPos / gridTileWidth);

        mapData[x].Y[y] = tileId;
    }

    /// <summary>
    /// Retrieve all objects of type T.
    /// https://answers.unity.com/questions/1425758/how-can-i-find-all-instances-of-a-scriptable-objec.html
    /// Answer by PizzaPie - 10 / 25 / 2017
    /// </summary>
    /// <typeparam name="T"></typeparam>
    /// <returns></returns>
    private T[] GetAllInstances<T>() where T : ScriptableObject
    {
        string[] guids = AssetDatabase.FindAssets("t:" + typeof(T).Name);
        T[] a = new T[guids.Length];
        for (int i = 0; i < guids.Length; i++)
        {
            string path = AssetDatabase.GUIDToAssetPath(guids[i]);
            a[i] = AssetDatabase.LoadAssetAtPath<T>(path);
        }

        return a;
    }

    private TileMapData GetMapDataById(int mapId)
    {
        TileMapData[] allMapsData = GetAllInstances<TileMapData>();

        for (int i = 0; i < allMapsData.Length; i++)
        {
            if (allMapsData[i].GetId() == mapId)
            {
                return allMapsData[i];
            }
        }

        return null;
    }

    private void RefreshLargestUniqueId()
    {
        largestUniqueId = 0;

        TileMapData[] allMapsData = GetAllInstances<TileMapData>();

        for (int i = 0; i < allMapsData.Length; i++)
        {
            if (allMapsData[i].GetId() > largestUniqueId)
            {
                largestUniqueId = allMapsData[i].GetId();
            }
        }
    }

    private int GetMapListViewElementIndexByMapId(int mapId)
    {
        int index = 0;

        for (int i = 0; i < tileMapsListView.itemsSource.Count; i++)
        {
            TileMapData newData = tileMapsListView.itemsSource[i] as TileMapData;

            if (newData.GetId() == mapId)
            {
                index = i;
                return index;
            }
        }

        return index;
    }

    private void CopyTileMapsData(ref TilesEditorData[] newData, TilesEditorData[] originalData)
    {
        for (int i = 0; i < originalData.Length; i++)
        {
            TileMapData originalMap = (originalData[i] as TileMapData);
            TileMapData newMap = CreateInstance<TileMapData>();

            if (originalMap == null) return;

            CopyTileMapData(ref newMap, originalMap);
            newData[i] = newMap;
        }
    }

    private void CopyTileMapData(ref TileMapData newData, TileMapData originalData)
    {
        newData.SetName(originalData.GetName());
        newData.SetId(originalData.GetId());

        TileMapData.MapVector[] originalMap = originalData.GetMapData();
        TileMapData.MapVector[] newMap = new TileMapData.MapVector[originalMap.Length];

        for (int j = 0; j < originalMap.Length; j++)
        {
            newMap[j].Y = new int[originalMap[j].Y.Length];

            for (int k = 0; k < originalMap[j].Y.Length; k++)
            {
                newMap[j].Y[k] = originalMap[j].Y[k];
            }
        }

        newData.SetMapData(newMap);
    }

    private void CopyTilesData(ref TilesEditorData[] newData, TilesEditorData[] originalData)
    {
        for (int i = 0; i < originalData.Length; i++)
        {
            if (originalData[i] as TileData == null) return;
            newData = originalData;
        }
    }
}