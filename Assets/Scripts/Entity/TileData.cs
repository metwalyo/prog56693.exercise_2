﻿using UnityEngine;

[CreateAssetMenu(fileName = "TileData", menuName = "My Assets/Tile Data")]
public class TileData : TilesEditorData
{
    [SerializeField] private Sprite tileSprite = null;

    public Sprite GetSprite()
    {
        return tileSprite;
    }
}
