﻿using UnityEngine;

public class TilesEditorData : ScriptableObject
{
    [SerializeField] private int id = 0;
    [SerializeField] private string objectName = "Object Name";

    public int GetId()
    {
        return id;
    }

    public string GetName()
    {
        return objectName;
    }

    public void SetName(string newName)
    {
        objectName = newName;
    }

    public void SetId(int newId)
    {
        id = newId;
    }
}
