﻿using UnityEngine;

[CreateAssetMenu(fileName = "TileData", menuName = "My Assets/Tile Map Data")]
public class TileMapData : TilesEditorData
{
    [HideInInspector] [SerializeField] private MapVector[] mapData = null;

    [System.Serializable]
    public struct MapVector
    {
        public int[] Y;
    }

    public void SetMapData(MapVector[] data)
    {
        mapData = data;
    }

    public MapVector[] GetMapData()
    {
        return mapData;
    }
}